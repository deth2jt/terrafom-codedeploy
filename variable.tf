
variable "name_tage" {
  type    = string
  default = "aws-codedeploy"
}

variable "env_tage" {
  type    = string
  default = "staging"
}